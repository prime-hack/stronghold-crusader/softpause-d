import core.sys.windows.windows, core.sys.windows.dll, core.runtime;
import config;
import llmo; 

public __gshared static stConfig!"SoftPause.ini" cfg;

extern(Windows) BOOL DllMain(HINSTANCE hInstance, ULONG ulReason, LPVOID pvReserved) {
    switch (ulReason) {
        case DLL_PROCESS_ATTACH:
            Runtime.initialize();
            init();
            dll_process_attach(hInstance, true);
            break;
        case DLL_PROCESS_DETACH:
            cfg.save();
            Runtime.terminate();
            dll_process_detach(hInstance, true);
            break;
        case DLL_THREAD_ATTACH:
            dll_thread_attach(true, true);
            break;
        case DLL_THREAD_DETACH:
            dll_thread_detach(true, true);
            break;
        
        default:
            break;
    }
    return true;
}

void init(){
    cfg.load();
    
    if ( cfg.allowPlaceBuildings ) {
        setMemory( 0x4445EA, 0x90, 6, MemorySafe.prot );
        setMemory( 0x445364, 0x90, 6, MemorySafe.prot );
    }
    
    if ( cfg.allowArmyControl ) {
        setMemory( 0x434AA8, 0x90, 6, MemorySafe.prot );
    }
    
    if ( cfg.allowActivateBuildings ) {
        setMemory( 0x463516, 0x90, 2, MemorySafe.prot );
    }
    
    if ( cfg.allowBook ) {
        setMemory( 0x44503A, 0x90, 6, MemorySafe.prot );
    }
}
