module config;

import std.file : exists, isFile;
import inifiled;

@INI("Main settings", "Config")
struct stConfig(string inifile){
	void load(){
		if (inifile.exists && inifile.isFile)
			this.readINIFile(inifile);
	}
	void save(){
		this.writeINIFile(inifile);
	}
	
	@INI("Allow place buildings in pause") bool allowPlaceBuildings;
	@INI("Allow control army in pause") bool allowArmyControl;
	@INI("Allow activate buildings in pause") bool allowActivateBuildings = true;
	@INI("Allow open book in pause") bool allowBook = true;
};
