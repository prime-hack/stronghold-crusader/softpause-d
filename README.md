[![pipeline status](https://gitlab.com/prime-hack/stronghold-crusader/softpause-d/badges/master/pipeline.svg)](https://gitlab.com/prime-hack/stronghold-crusader/softpause-d/-/commits/master)

## About SoftPause

Plugin make soft confines for pause in Stronghold Crusader HD. With this plugin you can allow next actions in pause:

- Place building
- Control army
- Activate buildings
- Open book

By default enabled only `activate buildings` and `open book`. You can change confines in configure file `SoftPause.ini`

## Install

Move file [SoftPause.asi](https://gitlab.com/prime-hack/stronghold-crusader/softpause-d/-/pipelines) to game folder

## Configure

Configure file was automatically created after first game run, and named `SoftPause.ini`

#### Default config

```ini
; Main settings
[Config]
; Allow place buildings in pause
allowPlaceBuildings="false"
; Allow control army in pause
allowArmyControl="false"
; Allow activate buildings in pause
allowActivateBuildings="true"
; Allow open book in pause
allowBook="true"
```

- **allowPlaceBuildings** - allow select and place buildings, when game is paused
- **allowArmyControl** - allow give orders for army and view hitpoints
- **allowActivateBuildings** - allow actvate buildings, when game is paused. E.g. main tower, shop, barracks, etc
- **allowBook** - allow activate book with reputation, when game in pause

## Build

```bash
dub build -brelease
```

#### Cross-compilation

For cross-compilation your need use **[LDC2](https://github.com/ldc-developers/ldc/releases)**
```bash
dub build -brelease --compiler=ldc2 --arch=i686-pc-windows-msvc
```

### After building

Rename `SoftPause.dll` to `SoftPause.asi`